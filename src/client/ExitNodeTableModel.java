/*
 * Copyright (C) 2009-2017 Alistair Neil <info@dazzleships.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alistair Neil <info@dazzleships.net>
 */
public class ExitNodeTableModel extends DefaultTableModel {

    private final List<String> salFinger = Collections.synchronizedList(new ArrayList<String>());
    private final String[] columnNames = new String[5];
    private NodeItem ni;

    public ExitNodeTableModel() {
    }

    /**
     * Clear the table model of all data
     */
    public void clear() {
        getDataVector().clear();
        salFinger.clear();
    }

    /**
     * Get the number of columns
     *
     * @return columns as integer
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * Test if the cell is editable
     *
     * @param row Cell row
     * @param col Cell column
     * @return True if editable
     */
    @Override
    public boolean isCellEditable(int row, int col) {
        return col == 4;
    }

    /**
     * Set value with given fingerprint at given column
     *
     * @param value Value
     * @param finger Fingerprint
     * @param col Column
     */
    public void setValueAt(Object value, String finger, int col) {
        setValueAt(value, salFinger.indexOf(finger), col);
    }

    /**
     * Convenience method to set the testing field cell values for a specific
     * fingerprint, the hops field is hidden and does npt show up in table
     *
     * @param finger Fingerprint
     * @param latency Latency value
     * @param status Status value
     * @param hops Hops value
     */
    public synchronized void setTestFieldValues(String finger, long latency, String status, String hops) {
        int row = salFinger.indexOf(finger);
        if (row > -1) {
            ni = (NodeItem) super.getValueAt(row, 0);
            ni.setTestLatency(latency);
            ni.setTestingMessage(status);
            ni.setCircuitHops(hops);
            fireTableRowsUpdated(row, row);
        }
    }

    /**
     * Get row nodeitem
     *
     * @param row
     * @return a nodeitem
     */
    public NodeItem getNodeItemAt(int row) {
        return (NodeItem) super.getValueAt(row, 0);
    }

    /**
     * Set cell value at row,col
     *
     * @param value
     * @param row
     * @param col
     */
    @Override
    public void setValueAt(Object value, int row, int col) {
        try {
            ni = (NodeItem) super.getValueAt(row, 0);
            switch (col) {
                case 1:
                    ni.setBandwidth((Float) value);
                    break;
                case 2:
                    ni.setTestLatency((Long) value);
                    break;
                case 3:
                    ni.setTestingMessage((String) value);
                    break;
                case 4:
                    ni.setFavouriteEnabled((Boolean) value);
                    break;
                case 5:
                    salFinger.set(row, (String) value);
                    ni.setFingerprint((String) value);
                    return;
                case 6:
                    ni.setCircuitHops((String) value);
                    return;
            }
            fireTableCellUpdated(row, col);
        } catch (Exception ex) {
        }
    }

    /**
     * Get cell value as object at row,col
     *
     * @param row
     * @param col
     * @return value as object
     */
    @Override
    public Object getValueAt(int row, int col) {
        ni = (NodeItem) super.getValueAt(row, 0);
        Object obj = null;
        switch (col) {
            case 0:
                obj = ni.getNickName();
                break;
            case 1:
                obj = ni.getBandwidth();
                break;
            case 2:
                obj = ni.getTestLatency();
                break;
            case 3:
                obj = ni.getTestingMessage();
                break;
            case 4:
                obj = ni.isFavourite();
                break;
            case 5:
                obj = ni.getFingerprint();
                break;
            case 6:
                obj = ni.getCircuitHops();
                break;
            case 7:
                obj = ni.getTestStatus();
                break;
        }
        return obj;
    }

    /**
     * Add rowdata with given data object array
     *
     * @param rowData
     */
    @Override
    public void addRow(Object[] rowData) {
        super.addRow(rowData);
        salFinger.add(((NodeItem) rowData[0]).getFingerprint());
    }

    /**
     * Get column name at given index
     *
     * @param index
     * @return name as string
     */
    @Override
    public String getColumnName(int index) {
        return columnNames[index];
    }

    /**
     * Set the column name at the specified index
     *
     * @param name
     * @param index
     */
    public void setColumnName(String name, int index) {
        if (index < columnNames.length) {
            columnNames[index] = name;
        }
    }

    /**
     * Get column class at given index
     *
     * @param index
     * @return Class
     */
    @Override
    public Class getColumnClass(int index) {
        Class cl;
        switch (index) {
            case 1:
                cl = Float.class;
                break;
            case 2:
                cl = Long.class;
                break;
            case 4:
                cl = Boolean.class;
                break;
            default:
                cl = String.class;
                break;
        }
        return cl;
    }

}
