/*
 * Copyright (C) 2009-2017 Alistair Neil <info@dazzleships.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package client;

import java.util.regex.Pattern;

/**
 *
 * @author Alistair Neil <info@dazzleships.net>
 */
public final class TorCircuit {

    public static final int FINGER = 0;
    public static final int NICKNAME = 1;
    private String[] dataCirc;
    private String[] dataHops;
    private String circuitinfo;

    /**
     * Constructor
     */
    public TorCircuit() {
    }

    /**
     * Constructor, Create circuit object with given circuit
     *
     * @param circuit
     */
    public TorCircuit(String circuit) {
        setCircuit(circuit);
    }

    /**
     * Set circuit to supplied circuit
     *
     * @param circuit
     */
    public void setCircuit(String circuit) {
        circuitinfo = circuit;
        Pattern pattern = Pattern.compile(" ");
        dataCirc = pattern.split(circuit);
        setHops(dataCirc[2]);
    }

    /**
     * Get circuit
     *
     * @return circuit as string
     */
    public String getCircuit() {
        return circuitinfo;
    }

    /**
     * Set hops
     *
     * @param hops
     */
    public void setHops(String hops) {
        Pattern pattern = Pattern.compile(",");
        dataHops = pattern.split(hops);
    }

    /**
     * Get current circuit ID
     *
     * @return ID as string
     */
    public String getID() {
        return dataCirc[0];
    }

    /**
     * Get current circuit status
     *
     * @return status as string
     */
    public String getStatus() {
        return dataCirc[1];
    }

    /**
     * Get current circuits build flags
     *
     * @return build flags as string
     */
    public String getBuildFlags() {
        return dataCirc[3].substring(12);
    }

    /**
     * Get current circuits purpose
     *
     * @return purpose as string
     */
    public String getPurpose() {
        return dataCirc[4].substring(8);
    }

    /**
     * Get current circuits timestamp
     *
     * @return timestamp as string
     */
    public String getTimestamp() {
        return dataCirc[5].substring(13);
    }

    /**
     * Get circuit hops
     *
     * @return circuit hops as string
     */
    public String getHops() {
        return dataCirc[2];
    }

    /**
     * Get current circuits guard or entry node
     *
     * @return guard node as string
     */
    public String getGuardHop() {
        String result = null;
        try {
            result = dataHops[0];
        } catch (Exception ex) {
        }
        return result;
    }

    /**
     * Get current circuits middle node if any, returns exit node for a two hop
     * circuit
     *
     * @return middleman node as string
     */
    public String getMiddleHop() {
        String result = null;
        try {
            if (dataHops.length < 3) {
                result = "";
            } else {
                result = dataHops[1];
            }
        } catch (Exception ex) {
        }
        return result;
    }

    /**
     * Get current circuits exit node
     *
     * @return exit node as string
     */
    public String getExitHop() {
        String result = null;
        try {
            result = dataHops[dataHops.length - 1];
        } catch (Exception ex) {
        }
        return result;
    }

    /**
     * Get Guardnode finger or nick
     *
     * @param type 0 = finger, 1 = nick
     * @return fingerprint as string
     */
    public String getGuard(int type) {
        return getFromNodeHop(getGuardHop(), type);
    }

    /**
     * Get Middleman
     *
     * @param type 0 = finger, 1 = nick
     * @return fingerprint as string
     */
    public String getMiddleMan(int type) {
        return getFromNodeHop(getMiddleHop(), type);
    }

    /**
     * Get Exitnode
     *
     * @param type 0 = finger, 1 = nick
     * @return fingerprint as string
     */
    public String getExit(int type) {
        return getFromNodeHop(getExitHop(), type);
    }

    /**
     * Get fingerprint of given node Nodeinfo is in the format of
     * fingerprint~name or fingerprint=name
     *
     * @param nodeinfo
     * @param type 0 = finger, 1 = nickname
     * @return fingerprint as string
     */
    public static String getFromNodeHop(String nodeinfo, int type) {
        Pattern pattern;
        try {
            if (nodeinfo.contains("~")) {
                pattern = Pattern.compile("~");
            } else {
                pattern = Pattern.compile("=");
            }
            String[] result = pattern.split(nodeinfo);
            if (result.length > 0) {
                return result[type];
            }
        } catch (Exception ex) {
        }
        return null;
    }

}
