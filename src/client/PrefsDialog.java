/*
 * Copyright (C) 2009-2017 Alistair Neil <info@dazzleships.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package client;

import javax.swing.DefaultComboBoxModel;
import lib.Localisation;
import lib.SimpleProps;
import lib.Utilities;

/**
 *
 * @author Alistair Neil, <info@dazzleships.net>
 */
public final class PrefsDialog extends javax.swing.JDialog {

    public static final int CANCEL = 0;
    public static final int APPLY = 1;
    public static final int RESET = 2;

    private static final Localisation LOCAL = new Localisation("resources/MessagesBundle");
    private final SimpleProps sp;
    private final String disabledprefs;

    /**
     * Creates a preferences editor dialog
     *
     * @param parent Parent Frame
     * @param sp
     * @param disabledprefs
     */
    public PrefsDialog(java.awt.Frame parent, SimpleProps sp, String disabledprefs) {
        super(parent, true);
        this.sp = sp;
        this.disabledprefs = disabledprefs;
        initComponents();
        loadBasicPrefs(false);
        loadAdvancedPrefs(false);
    }

    /**
     * @return the return status of this dialog - one of RET_OK or RET_CANCEL
     */
    public int getReturnStatus() {
        return returnStatus;
    }

    public void loadBasicPrefs(boolean usedefault) {

        sp.setDefaultModeEnabled(usedefault);
        jCheckAutostart.setSelected(sp.getBool("PREF_AUTOSTART"));
        jCheckDisableTray.setSelected(sp.getBool("PREF_NOSYSTRAY"));
        jCheckHideMin.setEnabled(!jCheckDisableTray.isSelected());
        jCheckHideTray.setSelected(sp.getBool("PREF_HIDETOTRAY"));
        jCheckAppUpdate.setSelected(sp.getBool("PREF_UPDATECHECK"));
        jCheckMinOnClose.setSelected(sp.getBool("PREF_MINONCLOSE"));
        jCheckCacheDelete.setSelected(sp.getBool("PREF_CACHEDELETE"));
        jCheckDisableNotify.setSelected(sp.getBool("PREF_DISABLE_NOTIFY"));
        jCheckHideMin.setSelected(sp.getBool("PREF_HIDE_MIN"));
        jCheckGeoUpdate.setSelected(sp.getBool("PREF_GEOCHECK"));
        if (disabledprefs.contains("jCheckDisableTray")) {
            jCheckDisableTray.setEnabled(false);
            jCheckHideMin.setEnabled(false);
        }
        jCheckAppUpdate.setVisible(!disabledprefs.contains("jCheckAppUpdate"));
        sp.setDefaultModeEnabled(false);
        updatePortRanges(sp.getInt("PREF_TOR_TESTHREADS"));
    }

    /**
     * Load the preferences
     *
     * @param usedefault
     */
    public void loadAdvancedPrefs(boolean usedefault) {
        sp.setDefaultModeEnabled(usedefault);
        jTextHTTPProxy.setText(sp.getString("PREF_HTTP_PROXY"));
        jSpinnerMainPort.setValue(sp.getInt("PREF_LISTENPORT"));
        jTextTorBridge.setText(sp.getString("PREF_TORBRIDGE"));
        jTextTorArgs.setText(sp.getString("PREF_TORARGS"));
        jComboLogLev.setSelectedIndex(sp.getInt("PREF_TORLOGLEV"));
        jCheckSafeSocks.setSelected(sp.getBool("PREF_SAFESOCKS"));
        jCheckTestSocks.setSelected(sp.getBool("PREF_TESTSOCKS"));
        jCheckAvoidDisk.setSelected(sp.getBool("PREF_AVOIDDISK"));
        jCheckSafeLog.setSelected(sp.getBool("PREF_SAFELOG"));
        sp.setDefaultModeEnabled(false);
    }

    /**
     * Save preferences
     */
    public void savePreferences() {

        sp.setInt("PREF_LISTENPORT", (Integer) jSpinnerMainPort.getValue());
        sp.setInt("PREF_TORLOGLEV", jComboLogLev.getSelectedIndex());
        sp.setBool("PREF_AUTOSTART", jCheckAutostart.isSelected());
        sp.setBool("PREF_NOSYSTRAY", jCheckDisableTray.isSelected());
        sp.setBool("PREF_HIDETOTRAY", jCheckHideTray.isSelected());
        sp.setBool("PREF_UPDATECHECK", jCheckAppUpdate.isSelected());
        sp.setBool("PREF_MINONCLOSE", jCheckMinOnClose.isSelected());
        sp.setBool("PREF_SAFESOCKS", jCheckSafeSocks.isSelected());
        sp.setBool("PREF_TESTSOCKS", jCheckTestSocks.isSelected());
        sp.setBool("PREF_AVOIDDISK", jCheckAvoidDisk.isSelected());
        sp.setBool("PREF_SAFELOG", jCheckSafeLog.isSelected());
        sp.setBool("PREF_CACHEDELETE", jCheckCacheDelete.isSelected());
        sp.setBool("PREF_DISABLE_NOTIFY", jCheckDisableNotify.isSelected());
        sp.setBool("PREF_HIDE_MIN", jCheckHideMin.isSelected());
        sp.setBool("PREF_GEOCHECK", jCheckGeoUpdate.isSelected());
        sp.setString("PREF_HTTP_PROXY", jTextHTTPProxy.getText());
        sp.setString("PREF_TORBRIDGE", jTextTorBridge.getText());
        sp.setString("PREF_TORARGS", jTextTorArgs.getText());
    }

    /**
     * Update displayed port ranges to reflect any port changes
     */
    private void updatePortRanges(int threads) {
        int intBasePort = sp.getInt("PREF_LISTENPORT");
        String strRanges = LOCAL.getString("label_portranges");
        strRanges = strRanges.replace("$portmin", String.valueOf(intBasePort));
        strRanges = strRanges.replace("$portmax", String.valueOf(intBasePort + (threads * 2) + 1));
        jLabelPortRanges.setText(strRanges);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabPrefs = new javax.swing.JTabbedPane();
        jPanelBasicPrefs = new javax.swing.JPanel();
        jPanelGeneralPrefs = new javax.swing.JPanel();
        jCheckHideTray = new javax.swing.JCheckBox();
        jCheckAppUpdate = new javax.swing.JCheckBox();
        jCheckAutostart = new javax.swing.JCheckBox();
        jCheckDisableTray = new javax.swing.JCheckBox();
        jCheckMinOnClose = new javax.swing.JCheckBox();
        jCheckCacheDelete = new javax.swing.JCheckBox();
        jCheckDisableNotify = new javax.swing.JCheckBox();
        jCheckHideMin = new javax.swing.JCheckBox();
        jCheckGeoUpdate = new javax.swing.JCheckBox();
        jPanelAdvancedPrefs = new javax.swing.JPanel();
        jPanelTorPrefs = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jSpinnerMainPort = new javax.swing.JSpinner();
        jLabelPortRanges = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTextTorBridge = new javax.swing.JTextField();
        jButtonGetBridges = new javax.swing.JButton();
        jCheckSafeSocks = new javax.swing.JCheckBox();
        jLabel12 = new javax.swing.JLabel();
        jCheckTestSocks = new javax.swing.JCheckBox();
        jLabel13 = new javax.swing.JLabel();
        jTextTorArgs = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jCheckAvoidDisk = new javax.swing.JCheckBox();
        jLabel15 = new javax.swing.JLabel();
        jComboLogLev = new javax.swing.JComboBox<>();
        jCheckSafeLog = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jTextHTTPProxy = new javax.swing.JTextField();
        jButtonCancel = new javax.swing.JButton();
        jButtonApply = new javax.swing.JButton();
        jButtonDefaults = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setIconImage(null);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jTabPrefs.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabPrefsStateChanged(evt);
            }
        });

        jPanelBasicPrefs.setFont(jPanelBasicPrefs.getFont().deriveFont(jPanelBasicPrefs.getFont().getStyle() | java.awt.Font.BOLD));

        jPanelGeneralPrefs.setBorder(javax.swing.BorderFactory.createTitledBorder(null, LOCAL.getString("panel_general"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, jPanelBasicPrefs.getFont(), jPanelBasicPrefs.getForeground())); // NOI18N

        jCheckHideTray.setText(LOCAL.getString("chkbox_hidetotray")); // NOI18N
        jCheckHideTray.setToolTipText(LOCAL.getString("ttip_hidetotray")); // NOI18N

        jCheckAppUpdate.setSelected(true);
        jCheckAppUpdate.setText(LOCAL.getString("chkbox_checkforupdates")); // NOI18N
        jCheckAppUpdate.setToolTipText(LOCAL.getString("ttip_checkforupdates")); // NOI18N

        jCheckAutostart.setText(LOCAL.getString("chkbox_autostart")); // NOI18N
        jCheckAutostart.setToolTipText(LOCAL.getString("ttip_autostart")); // NOI18N

        jCheckDisableTray.setText(LOCAL.getString("chkbox_disabletray")); // NOI18N
        jCheckDisableTray.setToolTipText(LOCAL.getString("ttip_disabletray")); // NOI18N
        jCheckDisableTray.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckDisableTrayItemStateChanged(evt);
            }
        });

        jCheckMinOnClose.setText(LOCAL.getString("chkbox_minonclose")); // NOI18N
        jCheckMinOnClose.setToolTipText(LOCAL.getString("ttip_minonclose")); // NOI18N

        jCheckCacheDelete.setText(LOCAL.getString("chkbox_securedelete")); // NOI18N
        jCheckCacheDelete.setToolTipText(LOCAL.getString("ttip_securedelete")); // NOI18N

        jCheckDisableNotify.setText(LOCAL.getString("chkbox_disablenotify")); // NOI18N
        jCheckDisableNotify.setToolTipText(LOCAL.getString("ttip_disablenotify")); // NOI18N

        jCheckHideMin.setText(LOCAL.getString("chkbox_hidemin")); // NOI18N
        jCheckHideMin.setToolTipText(LOCAL.getString("ttip_hidemin")); // NOI18N

        jCheckGeoUpdate.setSelected(true);
        jCheckGeoUpdate.setText(LOCAL.getString("chkbox_geocheck")); // NOI18N
        jCheckGeoUpdate.setToolTipText(LOCAL.getString("ttip_geocheck")); // NOI18N

        javax.swing.GroupLayout jPanelGeneralPrefsLayout = new javax.swing.GroupLayout(jPanelGeneralPrefs);
        jPanelGeneralPrefs.setLayout(jPanelGeneralPrefsLayout);
        jPanelGeneralPrefsLayout.setHorizontalGroup(
            jPanelGeneralPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGeneralPrefsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelGeneralPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckAutostart)
                    .addComponent(jCheckHideTray)
                    .addComponent(jCheckCacheDelete)
                    .addComponent(jCheckHideMin)
                    .addComponent(jCheckDisableNotify))
                .addGap(18, 18, 18)
                .addGroup(jPanelGeneralPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckAppUpdate)
                    .addComponent(jCheckGeoUpdate)
                    .addComponent(jCheckDisableTray)
                    .addComponent(jCheckMinOnClose))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        jPanelGeneralPrefsLayout.setVerticalGroup(
            jPanelGeneralPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGeneralPrefsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelGeneralPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckAutostart)
                    .addComponent(jCheckGeoUpdate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelGeneralPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckHideTray)
                    .addComponent(jCheckDisableTray))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelGeneralPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckHideMin)
                    .addComponent(jCheckMinOnClose))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelGeneralPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckCacheDelete)
                    .addComponent(jCheckAppUpdate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckDisableNotify)
                .addContainerGap(132, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelBasicPrefsLayout = new javax.swing.GroupLayout(jPanelBasicPrefs);
        jPanelBasicPrefs.setLayout(jPanelBasicPrefsLayout);
        jPanelBasicPrefsLayout.setHorizontalGroup(
            jPanelBasicPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBasicPrefsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelGeneralPrefs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelBasicPrefsLayout.setVerticalGroup(
            jPanelBasicPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBasicPrefsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelGeneralPrefs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabPrefs.addTab(LOCAL.getString("tab_basic"), jPanelBasicPrefs); // NOI18N

        jPanelTorPrefs.setBorder(javax.swing.BorderFactory.createTitledBorder(null, LOCAL.getString("panel_torclientset"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, jPanelBasicPrefs.getFont(), jPanelBasicPrefs.getForeground())); // NOI18N

        jLabel6.setText(LOCAL.getString("label_listenport")); // NOI18N

        jSpinnerMainPort.setModel(new javax.swing.SpinnerNumberModel(9054, 9054, 9999, 1));
        jSpinnerMainPort.setToolTipText(LOCAL.getString("ttip_listenport")); // NOI18N

        jLabelPortRanges.setText("Currently active port ranges, 9052 to 9063");

        jLabel11.setText(LOCAL.getString("label_bridgeaddress")); // NOI18N

        jTextTorBridge.setToolTipText(LOCAL.getString("ttip_bridgeaddress")); // NOI18N

        jButtonGetBridges.setText(LOCAL.getString("button_getbridges")); // NOI18N
        jButtonGetBridges.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGetBridgesActionPerformed(evt);
            }
        });

        jCheckSafeSocks.setText(LOCAL.getString("chkbox_safesocks")); // NOI18N
        jCheckSafeSocks.setToolTipText(LOCAL.getString("ttip_safesocks")); // NOI18N

        jLabel12.setText(LOCAL.getString("label_torlogging")); // NOI18N

        jCheckTestSocks.setText(LOCAL.getString("chkbox_testsocks")); // NOI18N
        jCheckTestSocks.setToolTipText(LOCAL.getString("ttip_testsocks")); // NOI18N

        jLabel13.setText(LOCAL.getString("label_torargs")); // NOI18N

        jTextTorArgs.setToolTipText(LOCAL.getString("ttip_extraargs")); // NOI18N

        jLabel14.setText(LOCAL.getString("label_torsocks")); // NOI18N

        jCheckAvoidDisk.setText(LOCAL.getString("chkbox_diskavoid")); // NOI18N
        jCheckAvoidDisk.setToolTipText(LOCAL.getString("ttip_avoiddisk")); // NOI18N

        jLabel15.setText(LOCAL.getString("label_diskoptions")); // NOI18N

        jComboLogLev.setModel(new DefaultComboBoxModel<>(new String[] {LOCAL.getString("combo_loglev1"),LOCAL.getString("combo_loglev2"),LOCAL.getString("combo_loglev3")}));
        jComboLogLev.setToolTipText(LOCAL.getString("ttip_combo_loglevel")); // NOI18N

        jCheckSafeLog.setText(LOCAL.getString("chkbox_safelog")); // NOI18N
        jCheckSafeLog.setToolTipText(LOCAL.getString("ttip_safelogging")); // NOI18N

        javax.swing.GroupLayout jPanelTorPrefsLayout = new javax.swing.GroupLayout(jPanelTorPrefs);
        jPanelTorPrefs.setLayout(jPanelTorPrefsLayout);
        jPanelTorPrefsLayout.setHorizontalGroup(
            jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTorPrefsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel11)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelTorPrefsLayout.createSequentialGroup()
                        .addComponent(jCheckSafeSocks)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckTestSocks))
                    .addComponent(jCheckAvoidDisk)
                    .addGroup(jPanelTorPrefsLayout.createSequentialGroup()
                        .addComponent(jComboLogLev, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckSafeLog))
                    .addGroup(jPanelTorPrefsLayout.createSequentialGroup()
                        .addComponent(jTextTorBridge, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonGetBridges))
                    .addGroup(jPanelTorPrefsLayout.createSequentialGroup()
                        .addComponent(jSpinnerMainPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelPortRanges))
                    .addComponent(jTextTorArgs))
                .addContainerGap())
        );
        jPanelTorPrefsLayout.setVerticalGroup(
            jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTorPrefsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabelPortRanges)
                    .addComponent(jSpinnerMainPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTextTorBridge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonGetBridges))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jCheckSafeSocks)
                    .addComponent(jCheckTestSocks))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jComboLogLev, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckSafeLog))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jCheckAvoidDisk))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelTorPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jTextTorArgs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, LOCAL.getString("panel_network"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, jPanelBasicPrefs.getFont(), jPanelBasicPrefs.getForeground())); // NOI18N

        jLabel16.setText(LOCAL.getString("label_defaultproxy")); // NOI18N

        jTextHTTPProxy.setToolTipText(LOCAL.getString("ttip_defaultproxy")); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextHTTPProxy)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jTextHTTPProxy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelAdvancedPrefsLayout = new javax.swing.GroupLayout(jPanelAdvancedPrefs);
        jPanelAdvancedPrefs.setLayout(jPanelAdvancedPrefsLayout);
        jPanelAdvancedPrefsLayout.setHorizontalGroup(
            jPanelAdvancedPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAdvancedPrefsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelAdvancedPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelTorPrefs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelAdvancedPrefsLayout.setVerticalGroup(
            jPanelAdvancedPrefsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAdvancedPrefsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelTorPrefs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabPrefs.addTab(LOCAL.getString("tab_advanced"), jPanelAdvancedPrefs); // NOI18N

        jButtonCancel.setText(LOCAL.getString("button_close")); // NOI18N
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        jButtonApply.setText(LOCAL.getString("button_apply")); // NOI18N
        jButtonApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonApplyActionPerformed(evt);
            }
        });

        jButtonDefaults.setText(LOCAL.getString("button_prefdefaults")); // NOI18N
        jButtonDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDefaultsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonDefaults)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonApply)
                .addContainerGap())
            .addComponent(jTabPrefs)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabPrefs)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonApply)
                    .addComponent(jButtonCancel)
                    .addComponent(jButtonDefaults))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonApplyActionPerformed
        savePreferences();
        updatePortRanges(sp.getInt("PREF_TOR_TESTHREADS"));
        doClose(APPLY);
    }//GEN-LAST:event_jButtonApplyActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        doClose(CANCEL);
    }//GEN-LAST:event_jButtonCancelActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(CANCEL);
    }//GEN-LAST:event_closeDialog

    private void jButtonGetBridgesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGetBridgesActionPerformed
        Utilities.openFileExternally("https://bridges.torproject.org/bridges?lang="
                + LOCAL.toWebLanguageTag());
    }//GEN-LAST:event_jButtonGetBridgesActionPerformed

    private void jButtonDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDefaultsActionPerformed
        switch (jTabPrefs.getSelectedIndex()) {
            // Basic tab
            case 0:
                loadBasicPrefs(true);
                break;
            // Advanced tab
            case 1:
                loadAdvancedPrefs(true);
                break;
        }
    }//GEN-LAST:event_jButtonDefaultsActionPerformed

    private void jTabPrefsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabPrefsStateChanged
        switch (jTabPrefs.getSelectedIndex()) {
            case 0:
                jButtonDefaults.setToolTipText(LOCAL.getString("ttip_resetbasicprefs"));
                break;
            case 1:
                jButtonDefaults.setToolTipText(LOCAL.getString("ttip_resetadvprefs"));
                break;
        }
    }//GEN-LAST:event_jTabPrefsStateChanged

    private void jCheckDisableTrayItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckDisableTrayItemStateChanged
        jCheckHideMin.setEnabled(!jCheckDisableTray.isSelected());
    }//GEN-LAST:event_jCheckDisableTrayItemStateChanged

    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonApply;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonDefaults;
    private javax.swing.JButton jButtonGetBridges;
    private javax.swing.JCheckBox jCheckAppUpdate;
    private javax.swing.JCheckBox jCheckAutostart;
    private javax.swing.JCheckBox jCheckAvoidDisk;
    private javax.swing.JCheckBox jCheckCacheDelete;
    private javax.swing.JCheckBox jCheckDisableNotify;
    private javax.swing.JCheckBox jCheckDisableTray;
    private javax.swing.JCheckBox jCheckGeoUpdate;
    private javax.swing.JCheckBox jCheckHideMin;
    private javax.swing.JCheckBox jCheckHideTray;
    private javax.swing.JCheckBox jCheckMinOnClose;
    private javax.swing.JCheckBox jCheckSafeLog;
    private javax.swing.JCheckBox jCheckSafeSocks;
    private javax.swing.JCheckBox jCheckTestSocks;
    private javax.swing.JComboBox<String> jComboLogLev;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelPortRanges;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelAdvancedPrefs;
    private javax.swing.JPanel jPanelBasicPrefs;
    private javax.swing.JPanel jPanelGeneralPrefs;
    private javax.swing.JPanel jPanelTorPrefs;
    private javax.swing.JSpinner jSpinnerMainPort;
    private javax.swing.JTabbedPane jTabPrefs;
    private javax.swing.JTextField jTextHTTPProxy;
    private javax.swing.JTextField jTextTorArgs;
    private javax.swing.JTextField jTextTorBridge;
    // End of variables declaration//GEN-END:variables
    private int returnStatus = CANCEL;
}
