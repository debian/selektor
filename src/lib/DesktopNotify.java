/*
 * Copyright (C) 2009-2017 Alistair Neil <info@dazzleships.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lib;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles desktop notifications
 *
 * @author Alistair Neil <info@dazzleships.net>
 */
public class DesktopNotify {

    private final String iconpath;
    private String notifytitle;
    private String notifybody;
    private String notifysendpath;
    private final SwingTrayIcon sti;
    private boolean enabled;
    private boolean supported = true;

    public DesktopNotify(SwingTrayIcon sti, String iconpath) {
        this.sti = sti;
        this.iconpath = iconpath;
        if (OSFunction.isLinux()) {
            this.notifysendpath = OSFunction.findFile("notify-send", "/usr/bin/");
            if (this.notifysendpath == null) {
                Logger.getGlobal().log(Level.WARNING, "notify-send not found.");
                this.supported = false;
            }
        }
    }

    /**
     * Is notifications supported
     *
     * @return true if supported
     */
    public boolean isSupported() {
        return supported;
    }

    /**
     * Are notifications enabled
     *
     * @return true if enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Disable/Enable notifications
     *
     * @param enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Set desktop notification title
     *
     * @param text
     */
    public void setNotificationTitle(String text) {
        notifytitle = text;
    }

    /**
     * Set desktop notification body
     *
     * @param text
     */
    public void setNotificationBody(String text) {
        notifybody = text;
    }

    /**
     * Raise a notification
     *
     */
    public void raiseNotification() {
        if (notifybody == null || notifytitle == null || !enabled) {
            return;
        }
        if (OSFunction.isWindows()) {
            if (sti != null) {
                sti.displayMessage(notifytitle, notifybody);
            }
        } else {
            if (notifysendpath != null) {
                OSFunction.launchProcess("notify-send", "-u", "normal",
                        "-i", iconpath, notifytitle, notifybody);
            }
        }
        notifybody = null;
    }

    public String getNotifySendPath() {
        return notifysendpath;
    }

}
